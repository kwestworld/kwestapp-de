Workflow
1) Create a parent object and manipulate it as necesarry.
2) Add the "Paintable" component to the object
3) Go to the component's interface and add all the materials this object uses to the "Affected Materials" list.

Export
1) Open the "Kwest" menu, and then "Export Paintables Info".
2) Add all the necessary prefabs that have the "Paintable" component to the list.
3) Press the "Export info" button.
4) Choose where to save the materials list.

Creating AssetBundle
1) There are two fields on the very bottom of the Inspector parent object. They have the value "None".
2) The first field should be filled with the name of the AssetBundle, while the second one shoul have "bundle" in it.
3) Open the "Kwest" menu and press the "Build Bundles" button.
4) Unity should build the AssetBundle object with the name you entered in the first field, and it will be in Assets/AssetBundles