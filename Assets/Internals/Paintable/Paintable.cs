﻿using System;
using System.Collections.Generic;
using UnityEngine;

// ReSharper disable once CheckNamespace
[ExecuteInEditMode]
public sealed class Paintable : MonoBehaviour
{
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once CollectionNeverUpdated.Global
    // ReSharper disable once FieldCanBeMadeReadOnly.Global
    [HideInInspector] public List<string> PathByIndex = new List<string>();

    // ReSharper disable once UnusedMember.Global
    public List<Material> AffectedMaterials = new List<Material>();

#if UNITY_EDITOR
    // ReSharper disable once UnusedMember.Global
    public void Update()
    {
        var style = Style.ActiveStyle;
        if (style == null)
        {
            return;
        }
        
        for (var i = 0; i < PathByIndex.Count; i++)
        {
            var path = PathByIndex[i];
            Color color;

            if (!style.MaterialColors.TryGetValue(path, out color))
            {
                continue;
            }

            if (AffectedMaterials[i].color != color)
            {
                AffectedMaterials[i].color = color;
            }
        }
    }
#endif
}
