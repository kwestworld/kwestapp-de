﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public sealed class Style
{
#if UNITY_EDITOR
    public static Style ActiveStyle { get; set; }
#endif

    public string Name { get; set; }
    public Dictionary<string, Color> MaterialColors { get; set; }

    public Style()
    {
        Name = "New Style";
        MaterialColors = new Dictionary<string, Color>();
    }

    public string Serialize()
    {
        var ss = new SerializableStyle
        {
            Name = Name,
            MapStyle = "mapbox://styles/fahmidkwest/cj7lagh3a88732rlsmkg5sh1r",
            MaterialStyles = new List<MaterialStyle>()
            
        };

        foreach (var materialColor in MaterialColors)
        {
            var color = materialColor.Value;
            ss.MaterialStyles.Add(new MaterialStyle
            {
                Path = materialColor.Key,
                Color = new[] {color.r, color.g, color.b, color.a}
            });
        }

        return JsonConvert.SerializeObject(ss);
    }
}

public sealed class SerializableStyle
{
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("mapStyle")]
    public string MapStyle { get; set; }
    
    [JsonProperty("MaterialStyles")]
    public List<MaterialStyle> MaterialStyles { get; set; }
}

public sealed class MaterialStyle
{
    public string Path;
    public float[] Color;
}