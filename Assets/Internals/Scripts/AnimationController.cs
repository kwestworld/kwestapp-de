﻿using UnityEngine;

[ExecuteInEditMode]
public class AnimationController : MonoBehaviour
{
    public void BackToIdle()
    {
        var animator = GetComponent<Animator>();
        if (animator == null) return;

        animator.SetInteger("state", /* State.Idle */ 0);
    }
}