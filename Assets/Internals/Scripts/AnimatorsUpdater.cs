﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif
public class AnimatorsUpdater : MonoBehaviour
{
#if UNITY_EDITOR
    public void OnEnable()
    {
        EditorApplication.update += EditorUpdate;
    }

    public void OnDisable()
    {
        // ReSharper disable once DelegateSubtraction
        EditorApplication.update -= EditorUpdate;
    }

    private void EditorUpdate()
    {
        var animators = gameObject.GetComponentsInChildren<Animator>();

        foreach (var animator in animators)
        {
            animator.Update(Time.deltaTime);
        }
    }
#endif
}