﻿using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Data;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
[ExecuteInEditMode]
public sealed class MapPreview : BasicMap
{
    [HideInInspector] public bool IsDirty;

    protected override void Awake()
    {
        foreach (var tile in GetComponentsInChildren<UnityTile>())
        {
            var t = tile.transform;
            for (var i = t.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(t.GetChild(i).gameObject);
            }
        }

        base.Awake();
    }

    public void Update()
    {
        if (!IsDirty) return;
        IsDirty = false;
        Reset();
    }
}

#if UNITY_EDITOR

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
[CustomEditor(typeof(MapPreview))]
public sealed class MapPreviewEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (!GUILayout.Button("Refresh")) return;
        var isDirty = serializedObject.FindProperty("IsDirty");
        isDirty.boolValue = true;
        serializedObject.ApplyModifiedPropertiesWithoutUndo();

        var zoom = serializedObject.FindProperty("_zoom");

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Play Mode zoom"))
        {
            zoom.floatValue = 18f;
        }

        if (GUILayout.Button("World Mode zoom"))
        {
            zoom.floatValue = 12f;
        }

        EditorGUILayout.EndHorizontal();
    }
}

#endif