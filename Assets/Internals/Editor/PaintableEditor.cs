﻿using UnityEditor;

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
[CustomEditor(typeof(Paintable))]
public class PaintableEditor : Editor
{
    private SerializedProperty m_affectedMaterials;
    private SerializedProperty m_pathByIndex;

    // ReSharper disable once UnusedMember.Global
    public void Awake()
    {
        m_affectedMaterials = serializedObject.FindProperty("AffectedMaterials");
        m_pathByIndex = serializedObject.FindProperty("PathByIndex");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        m_pathByIndex.arraySize = m_affectedMaterials.arraySize;
        for (var i = 0; i < m_pathByIndex.arraySize; i++)
        {
            var path = m_pathByIndex.GetArrayElementAtIndex(i);
            var material = m_affectedMaterials.GetArrayElementAtIndex(i);

            path.stringValue = AssetDatabase.GetAssetPath(material.objectReferenceValue);
        }
        serializedObject.ApplyModifiedProperties();
    }
}