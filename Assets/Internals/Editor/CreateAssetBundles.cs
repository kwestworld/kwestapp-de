﻿using System.IO;
using UnityEditor;

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
public static class CreateAssetBundles
{
    private const string AssetBundleDirectory = "Assets/AssetBundles";

    // ReSharper disable once UnusedMember.Global
    [MenuItem("Kwest/Build Bundles")]
    public static void BuildBundles()
    {
        if (!Directory.Exists(AssetBundleDirectory))
        {
            Directory.CreateDirectory(AssetBundleDirectory);
        }

        BuildPipeline.BuildAssetBundles(
            AssetBundleDirectory, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget
        );
    }
}