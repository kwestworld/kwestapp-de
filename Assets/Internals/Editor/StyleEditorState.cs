﻿// ReSharper disable once CheckNamespace
public abstract class StyleEditorState
{
    public StyleEditor Editor { get; set; }

    public virtual void Init()
    {
    }

    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once InconsistentNaming
    public abstract void OnGUI();
}