﻿using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public sealed class MenuEditorState : StyleEditorState
{
    public override void OnGUI()
    {
        EditorGUILayout.LabelField("Select object for preview and edit:");
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Use Map"))
        {
            UseMap();
        }

        if (GUILayout.Button("Use Prefab"))
        {
            var id = GUIUtility.GetControlID(FocusType.Passive);
            EditorGUIUtility.ShowObjectPicker<GameObject>(null, false, "", id);
        }

        EditorGUILayout.EndHorizontal();

        if (Event.current.commandName != "ObjectSelectorClosed") return;
        var obj = EditorGUIUtility.GetObjectPickerObject() as GameObject;
        Use(obj);
    }

    private void UseMap()
    {
        var mapAsset = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/MapPrefab.prefab");
        Use(mapAsset);
    }

    private void Use(GameObject obj)
    {
        if (obj == null)
        {
            return;
        }

        Editor.State = new EditEditorState(obj)
        {
            Editor = Editor
        };
        Editor.State.Init();
        Editor.Repaint();
    }
}