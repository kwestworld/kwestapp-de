﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

// ReSharper disable once ClassNeverInstantiated.Global
// ReSharper disable once CheckNamespace
[SuppressMessage("ReSharper", "UnusedMember.Global")]
public sealed class StyleEditor : EditorWindow
{
    [MenuItem("Kwest/Open style editor")]
    public static void OpenEditor()
    {
        GetWindow<StyleEditor>();
    }

    public GameObject Root { get; private set; }
    public StyleEditorState State { get; set; }

    private static void RunAfterReload(Scene scene, LoadSceneMode mode)
    {
        EditorSceneManager.sceneLoaded -= RunAfterReload;
        OpenEditor();
    }

    public void Awake()
    {
        // Trying to find root
        var scene = SceneManager.GetActiveScene();
        var root = scene.GetRootGameObjects().FirstOrDefault(go => go.name == "Root");

        if (root == null)
        {
            if (SceneManager.GetActiveScene().name == "Preview")
            {
                EditorUtility.DisplayDialog("Package broken", "Seems like this editor package is broken.\n" +
                                                              "Try again with clean install.\n" +
                                                              "If problem persist, please report to developer", "Ok");
                Close();
                return;
            }
            
            EditorSceneManager.sceneLoaded += RunAfterReload;
            EditorSceneManager.OpenScene("Assets/Resources/Scenes/Preview.unity");
            return;
        }

        // Cleaning root and make it usable
        Root = root;
        ResetRoot();

        State = new MenuEditorState
        {
            Editor = this
        };
    }

    public void OnDestroy()
    {
        ResetRoot();
    }

    public void ResetRoot()
    {
        if (Root == null) return;
        Root.SetActive(true);

        var transform = Root.transform;
        for (var i = transform.childCount - 1; i >= 0; i--)
        {
            var child = transform.GetChild(i).gameObject;
            DestroyImmediate(child);
        }
    }

    // ReSharper disable once InconsistentNaming
    public void OnGUI()
    {
        if (State == null)
        {
            Awake();
        }
        State.OnGUI();
    }
}