﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Mapbox.Unity.MeshGeneration.Factories;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
public sealed class EditEditorState : StyleEditorState
{
    private enum State { Encounter, Watch }
    private enum WatchState { Idle, PerformingAction, Outro }
    private enum EncounterState { Idle, Fail, Success, Lose, Win }

    private GameObject Prefab { get; set; }
    private GameObject Object { get; set; }
    private GameObject BackMap { get; set; }

    private State m_state = State.Encounter;
    private bool m_isMap;
    private bool m_isMapAttached;
    private bool m_isForceAnimation;
    
    private static readonly WatchState[] s_watchStates = { WatchState.Idle, WatchState.PerformingAction, WatchState.Outro};
    private static readonly EncounterState[] s_encounterStates = { EncounterState.Idle, EncounterState.Fail, EncounterState.Success, EncounterState.Lose, EncounterState.Win };

    public EditEditorState(GameObject obj)
    {
        Prefab = obj;
    }

    public override void Init()
    {
        var root = Editor.Root;
        Object = UnityEngine.Object.Instantiate(Prefab, root.transform);

        Style.ActiveStyle = new Style();
        m_isMap = Object.GetComponent<MapPreview>() != null;
    }

    public override void OnGUI()
    {
        var style = Style.ActiveStyle;
        var materials = new HashSet<string>();
        var paintables = Object.GetComponentsInChildren<Paintable>();

        foreach (var paintable in paintables)
        {
            materials.UnionWith(paintable.PathByIndex);
        }

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField(Prefab, typeof(GameObject), false);
        EditorGUI.EndDisabledGroup();

        style.Name = EditorGUILayout.TextField("Name:", style.Name);

        EditorGUILayout.BeginHorizontal();

        // If m_isMap is false, right expression won't be executed
        // and Button "Save to Prefab" won't appear.
        if (!m_isMap && GUILayout.Button("Save to Prefab"))
        {
            SavePrefab();
        }

        if (GUILayout.Button("Reload from Prefab"))
        {
            Editor.ResetRoot();
            Init();
        }

        if (GUILayout.Button("Close"))
        {
            Editor.Awake(); // hacky, but fast
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Export AssetBundle"))
        {
            ExportAssetBundle();
        }

        if (GUILayout.Button("Export style"))
        {
            ExportStyle();
        }
        EditorGUILayout.EndHorizontal();

        if (!m_isMap)
        {
            m_isMapAttached = EditorGUILayout.ToggleLeft("Attach map to preview", m_isMapAttached);
            EditorGUILayout.Space();

            if (m_isMapAttached && !BackMap)
            {
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources/Prefabs/MapPrefab.prefab");
                BackMap = UnityEngine.Object.Instantiate(prefab, Editor.Root.transform);
            }
            else if (!m_isMapAttached && BackMap)
            {
                UnityEngine.Object.DestroyImmediate(BackMap);
                BackMap = null;
            }
        }

        DrawAnimator();
        DrawMaterials(style, materials);

        foreach (var paintable in paintables)
        {
            paintable.Update();
        }
    }

    private void DrawAnimator()
    {
        if (Object == null)
        {
            return;
        }

        var animator = Object.GetComponentInChildren<Animator>();
        if (animator == null || animator.runtimeAnimatorController == null)
        {
            return;
        }
        //PrepareAnimator(animator);

        EditorGUILayout.LabelField("Animation Preview");
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Encounter"))
        {
            m_state = State.Encounter;
        }

        if (GUILayout.Button("Watch"))
        {
            m_state = State.Watch;
        }
        
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        m_isForceAnimation = EditorGUILayout.ToggleLeft("Force animation", m_isForceAnimation);

        if (GUILayout.Button("Intro"))
        {
            animator.Rebind();
        }

        switch (m_state)
        {
            case State.Encounter:
                DrawEncounterAnimator(animator);
                break;
            case State.Watch:
                DrawWatchAnimator(animator);
                break;
        }

        EditorGUILayout.Space();
    }

    private void DrawEncounterAnimator(Animator animator)
    {
        foreach (var state in s_encounterStates)
        {
            if (!GUILayout.Button(state.ToString())) continue;
            if (m_isForceAnimation)
            {
                animator.Play(state.ToString());
            }
            else
            {
                animator.SetInteger("state", (int)state);
            }
        }
    }

    private void DrawWatchAnimator(Animator animator)
    {
        foreach (var state in s_watchStates)
        {
            if (!GUILayout.Button(state.ToString())) continue;
            if (m_isForceAnimation)
            {
                animator.Play(state.ToString());
            }
            else
            {
                animator.SetInteger("state", (int)state);
            }
        }
    }

    private static void PrepareAnimator(Animator animator)
    {
        // This is example what happens
        // if your company develops a game engine
        // without QA.
#if UNITY_EDITOR
        const int failIndex = 4;
        const int successIndex = 2;
#else
        const int failIndex = 1;
        const int successIndex = 4;
#endif

        var clipInfos = animator.runtimeAnimatorController.animationClips;
        var failClip = clipInfos[failIndex];
        var successClip = clipInfos[successIndex];

        if (failClip != null)
        {
            CheckAndFixClip(failClip);
        }

        if (successClip != null)
        {
            CheckAndFixClip(successClip);
        }
    }

    private static void CheckAndFixClip(AnimationClip clip)
    {
        var events = AnimationUtility.GetAnimationEvents(clip);
        foreach (var @event in events)
        {
            if (@event.functionName != "BackToIdle") continue;
            if (@event.time == 0f) return;
            @event.time = 0f;
            return;
        }

        Array.Resize(ref events, events.Length + 1);
        events[events.Length - 1] = new AnimationEvent
        {
            functionName = "BackToIdle",
            time = 0f
        };
        AnimationUtility.SetAnimationEvents(clip, events);
    }

    private static void DrawMaterials(Style style, IEnumerable<string> materials)
    {
        EditorGUILayout.LabelField("Materials:");
        EditorGUI.indentLevel++;
        foreach (var material in materials)
        {
            EditorGUILayout.LabelField(material);

            if (style.MaterialColors.ContainsKey(material))
            {
                style.MaterialColors[material] = EditorGUILayout.ColorField("Color:", style.MaterialColors[material]);
            }
            else
            {
                style.MaterialColors[material] = EditorGUILayout.ColorField("Color:", Color.white);
            }

            EditorGUILayout.Space();
        }
        EditorGUI.indentLevel--;
    }

    private void ExportAssetBundle()
    {
        var path = EditorUtility.SaveFilePanel("Export asset bundle...", "", Style.ActiveStyle.Name, "");
        if (string.IsNullOrEmpty(path)) return;

        var name = Path.GetFileName(path);
        path = Path.GetDirectoryName(path);

        var assets = m_isMap ? ExportMap() : ExportPrefab();

        var buildMap = new[]
        {
            new AssetBundleBuild
            {
                assetBundleName = name + ".android.bundle",
                assetNames = assets
            }
        };

        BuildPipeline.BuildAssetBundles(path, buildMap, BuildAssetBundleOptions.None,
            BuildTarget.Android);

        buildMap[0].assetBundleName = name + ".ios.bundle";
        BuildPipeline.BuildAssetBundles(path, buildMap, BuildAssetBundleOptions.None,
            BuildTarget.iOS);
    }

    private string[] ExportMap()
    {
        var assets = new HashSet<string>();
        var map = Object.GetComponentInChildren<MapPreview>();

        foreach (var paintable in Object.GetComponentsInChildren<Paintable>())
        {
            assets.UnionWith(paintable.PathByIndex);
        }

        foreach (var factory in map._mapVisualizer.Factories)
        {
            var terrainFactory = factory as TerrainFactory;
            if (terrainFactory == null) continue;

            assets.Add(AssetDatabase.GetAssetPath(terrainFactory._baseMaterial));
            break;
        }

        return assets.ToArray();
    }

    private string[] ExportPrefab()
    {
        var assets = new HashSet<string>
        {
            AssetDatabase.GetAssetPath(Prefab)
        };
        SavePrefab();

        foreach (var paintable in Object.GetComponentsInChildren<Paintable>())
        {
            assets.UnionWith(paintable.PathByIndex);
        }

        var animator = Object.GetComponentInChildren<Animator>();
        if (animator == null || animator.runtimeAnimatorController == null) return assets.ToArray();

        var controller = animator.runtimeAnimatorController;
        assets.Add(AssetDatabase.GetAssetPath(controller));

        foreach (var clip in controller.animationClips)
        {
            assets.Add(AssetDatabase.GetAssetPath(clip));
        }

        return assets.ToArray();
    }

    private static void ExportStyle()
    {
        var style = Style.ActiveStyle;
        var path = EditorUtility.SaveFilePanel("Export Style...", "", style.Name + ".style", "style");

        if (string.IsNullOrEmpty(path)) return;
        File.WriteAllText(path, style.Serialize());
    }

    private void SavePrefab()
    {
        var path = AssetDatabase.GetAssetPath(Prefab);
        PrefabUtility.ReplacePrefab(Object, Prefab);

        Prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
    }
}