﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
public class PaintableExportWindow : EditorWindow
{
    // ReSharper disable once UnusedMember.Global
    [MenuItem("Kwest/Export Paintables Info")]
    public static void ShowWindow()
    {
        var window = GetWindow<PaintableExportWindow>();
        window.Show();
    }

    private Paintable[] m_paintables = new Paintable[0];
    private readonly HashSet<string> m_paths = new HashSet<string>();

    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once InconsistentNaming
    public void OnGUI()
    {
        m_paths.Clear();
        {
            var len = EditorGUILayout.IntField("Size", m_paintables.Length);
            if (len != m_paintables.Length)
            {
                Array.Resize(ref m_paintables, len);
            }
        }

        for (var i = 0; i < m_paintables.Length; i++)
        {
            m_paintables[i] = EditorGUILayout.ObjectField(m_paintables[i], typeof(Paintable), true) as Paintable;
        }

        EditorGUILayout.Space();
        foreach (var paintable in m_paintables)
        {
            if (paintable == null) continue;
            m_paths.UnionWith(paintable.PathByIndex);

        }
        if (GUILayout.Button("Export info"))
        {
            Export();
        }

        EditorGUILayout.LabelField("Paths:");
        foreach (var path in m_paths)
        {
            EditorGUILayout.LabelField("    " + path);
        }
    }

    private void Export()
    {
        var path = EditorUtility.SaveFilePanel("Export info", null, "style.manifest", "manifest");
        if (string.IsNullOrEmpty(path)) return;

        var data = JsonConvert.SerializeObject(m_paths);
        File.WriteAllText(path, data);
    }
}